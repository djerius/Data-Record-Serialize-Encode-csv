# NAME

Data::Record::Serialize::Encode::csv - encode a record as csv

# VERSION

version 0.04

# SYNOPSIS

    use Data::Record::Serialize;

    my $s = Data::Record::Serialize->new( encode => 'csv', ... );

    $s->send( \%record );

# DESCRIPTION

**Data::Record::Serialize::Encode::csv** encodes a record as CSV (well
anything that [Text::CSV](https://metacpan.org/pod/Text%3A%3ACSV) can write).

It performs the [Data::Record::Serialize::Role::Encode](https://metacpan.org/pod/Data%3A%3ARecord%3A%3ASerialize%3A%3ARole%3A%3AEncode) role. If the data sink
is a stream, try [Data::Record::Serialize::Encode::csv\_stream](https://metacpan.org/pod/csv_stream); it
provides better performance using [Text::CSV](https://metacpan.org/pod/Text%3A%3ACSV)'s native output to filehandles.

# CONSTRUCTOR OPTIONS

## Text::CSV Options

Please see [Data::Record::Serialize::Role::Encode::CSV](https://metacpan.org/pod/Data%3A%3ARecord%3A%3ASerialize%3A%3ARole%3A%3AEncode%3A%3ACSV).

# SUPPORT

## Bugs

Please report any bugs or feature requests to bug-data-record-serialize-encode-csv@rt.cpan.org  or through the web interface at: https://rt.cpan.org/Public/Dist/Display.html?Name=Data-Record-Serialize-Encode-csv

## Source

Source is available at

    https://gitlab.com/djerius/data-record-serialize-encode-csv

and may be cloned from

    https://gitlab.com/djerius/data-record-serialize-encode-csv.git

# AUTHOR

Diab Jerius <djerius@cpan.org>

# COPYRIGHT AND LICENSE

This software is Copyright (c) 2022 by Smithsonian Astrophysical Observatory.

This is free software, licensed under:

    The GNU General Public License, Version 3, June 2007
