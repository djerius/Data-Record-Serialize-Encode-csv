package Data::Record::Serialize::Role::Encode::CSV;

# ABSTRACT: encode a record as csv

use Moo::Role;

use Data::Record::Serialize::Error { errors => ['csv_backend'] }, -all;
use Types::Common::String qw( NonEmptyStr );
use Types::Standard qw( Bool );

use Text::CSV;

use namespace::clean;

our $VERSION = '0.04';

sub _needs_eol { 1 }

has binary => (
    is      => 'ro',
    isa     => Bool,
    default => 1,
);

has sep_char => (
    is      => 'ro',
    default => ','
);

has quote_char => (
    is      => 'ro',
    default => '"'
);

has escape_char => (
    is      => 'ro',
    default => '"'
);

has always_quote => (
    is      => 'ro',
    isa     => Bool,
    default => 0,
);

has quote_empty => (
    is      => 'ro',
    isa     => Bool,
    default => 0,
);

has _csv => (
    is       => 'lazy',
    init_arg => undef,
);

sub to_bool { $_[0] ? 1 : 0 }

sub _build__csv {
    my $self = shift;

    my %args = (
        binary       => $self->binary,
        sep_char     => $self->sep_char,
        quote_char   => $self->quote_char,
        escape_char  => $self->escape_char,
        always_quote => $self->always_quote,
        quote_empty  => $self->quote_empty,
        auto_diag => 2,
    );

    return Text::CSV->new( \%args );
}

sub setup {
    my $self = shift;
    $self->_csv->combine( @{ $self->output_fields } )
      or croak( 'error creating CSV header' );
    $self->say( $self->_csv->string );
}

=for Pod::Coverage
 encode
 send
 setup
 to_bool

=cut


1;

# COPYRIGHT

__END__


=head1 SYNOPSIS

    use Data::Record::Serialize;

    my $s = Data::Record::Serialize->new( encode => 'csv', ... );

    $s->send( \%record );

=head1 DESCRIPTION

B<Data::Record::Serialize::Encode::csv> encodes a record as CSV (well
anything that L<Text::CSV> can write).

It performs the L<Data::Record::Serialize::Role::Encode> role.

=head1 CONSTRUCTOR OPTIONS

=head2 L<Text::CSV> Options

These are passed through to L<Text::CSV>:

=over

=item binary => I<Boolean>

Default: I<true>

=item sep_char => I<character>

Default: C<,>

=item quote_char => I<character>

Default: C<">

=item escape_char => i<character>

Default: C<">

=item always_quote => I<Boolean>

Default: I<false>
q

=item quote_empty => I<Boolean>

Default: I<false>

=back
