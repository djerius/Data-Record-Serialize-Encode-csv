package Data::Record::Serialize::Encode::csv_stream;

# ABSTRACT: encode a record as csv

use Moo::Role;

use namespace::clean;

our $VERSION = '0.04';


sub send {
    my $self = shift;
    $self->_csv->say( $self->fh, [ @{ $_[0] }{ @{ $self->output_fields } }  ]);
}

sub say {
    my $self = shift;
    $self->fh->say( @_ );
}

sub print {
    my $self = shift;
    $self->fh->print( @_ );
}

with 'Data::Record::Serialize::Role::Encode::CSV';
with 'Data::Record::Serialize::Role::Sink::Stream';

=for Pod::Coverage
encode
send
say
print

=cut

with 'Data::Record::Serialize::Role::EncodeAndSink';

1;

# COPYRIGHT

__END__


=head1 SYNOPSIS

    use Data::Record::Serialize;

    my $s = Data::Record::Serialize->new( encode => 'csv_stream', ... );

    $s->send( \%record );

=head1 DESCRIPTION

B<Data::Record::Serialize::Encode::csv_stream> encodes a record as CSV (well
anything that L<Text::CSV> can write) and writes it to a stream.

It performs both the L<Data::Record::Serialize::Role::Encode> and
L<Data::Record::Serialize::Role::Sink> roles.

It is more efficient than coupling the L<Data::Record::Serialize::Encode::csv|csv>
encoder with the B<Data::Record::Serialize::Sink::stream|stream> sink.

=head1 CONSTRUCTOR OPTIONS

=head2 Text::CSV Options

Please see L<Data::Record::Serialize::Role::Encode::CSV>.

=head2 Stream Options

Please see L<Data::Record::Serialize::Role::Sink::Stream>.

