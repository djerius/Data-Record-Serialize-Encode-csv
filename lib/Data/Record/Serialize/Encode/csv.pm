package Data::Record::Serialize::Encode::csv;

# ABSTRACT: encode a record as csv

use Moo::Role;

use namespace::clean;

our $VERSION = '0.04';

with 'Data::Record::Serialize::Role::Encode::CSV';

sub encode {
    my $self = shift;
    $self->_csv->combine( @{ $_[0] }{ @{ $self->output_fields } } );
    $self->_csv->string;
}

=for Pod::Coverage
 encode

=cut

with 'Data::Record::Serialize::Role::Encode';

1;

# COPYRIGHT

__END__


=head1 SYNOPSIS

    use Data::Record::Serialize;

    my $s = Data::Record::Serialize->new( encode => 'csv', ... );

    $s->send( \%record );

=head1 DESCRIPTION

B<Data::Record::Serialize::Encode::csv> encodes a record as CSV (well
anything that L<Text::CSV> can write).

It performs the L<Data::Record::Serialize::Role::Encode> role. If the data sink
is a stream, try L<Data::Record::Serialize::Encode::csv_stream|csv_stream>; it
provides better performance using L<Text::CSV>'s native output to filehandles.

=head1 CONSTRUCTOR OPTIONS

=head2 Text::CSV Options

Please see L<Data::Record::Serialize::Role::Encode::CSV>.
